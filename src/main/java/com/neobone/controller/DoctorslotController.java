package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Clinics;
import com.neobone.model.Doctorslot;
import com.neobone.model.Users;
import com.neobone.repository.ClinicsRepo;
import com.neobone.repository.DoctorslotRepo;
import com.neobone.repository.UsersRepo;


@Controller
public class DoctorslotController {
@Autowired
DoctorslotRepo doctorslotrepo;

@Autowired
UsersRepo usersrepo;

@Autowired
ClinicsRepo clinicsrepo;

@RequestMapping(value="/doctorslotpage", method=RequestMethod.POST)
public String doctorslotDetail(){
	return "doctorslot";
	
}

@RequestMapping(value="/doctorslots", method=RequestMethod.POST)
@ResponseBody
public Doctorslot doctorslotDetails(Doctorslot doctorslot) {
	 Users getslotuserid=usersrepo.findById(Long.parseLong(doctorslot.getSlotuserid())).get();
	 Clinics getslotclinicid=clinicsrepo.findById(Long.parseLong(doctorslot.getSlotclinicid())).get();
	 doctorslot.setUsers(getslotuserid);
	 doctorslot.setClinics(getslotclinicid);
	doctorslotrepo.save(doctorslot);
	return doctorslot;
}
}
