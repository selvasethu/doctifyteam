package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Patients;
import com.neobone.repository.PatientsRepo;

@Controller
public class PatientsController {
@Autowired
PatientsRepo patientsrepo;

@RequestMapping(value="/", method=RequestMethod.POST)
public String patients(){
	return "patients";
	
}

@RequestMapping(value="/patients", method=RequestMethod.POST)
@ResponseBody
public String patientDetails(Patients patientsmodel) {
	
	patientsrepo.save(patientsmodel);
	return "patients";
}
}
