package com.neobone.model;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Table
@Entity
public class PatientFiles {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long fileid;

@OneToOne(cascade = CascadeType.ALL)
@JoinColumn(name = "patientsid", referencedColumnName = "patientid")
private Patients patientdetail;

@Transient
private String filePatientid;



public String getFilePatientid() {
	return filePatientid;
}

public void setFilePatientid(String filePatientid) {
	this.filePatientid = filePatientid;
}

@Column
private String filePath;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime addedTime = ZonedDateTime.now();

@Column
private int addedUserid;
 
@Column
private String remark;

@OneToOne(cascade = CascadeType.ALL)
@JoinColumn(name = "apptid", referencedColumnName = "apptid")
private Appointments appointments;

@Transient
private String fileApptid;



public String getFileApptid() {
	return fileApptid;
}

public void setFileApptid(String fileApptid) {
	this.fileApptid = fileApptid;
}

public Long getFileid() {
	return fileid;
}

public void setFileid(Long fileid) {
	this.fileid = fileid;
}

public Patients getPatientdetail() {
	return patientdetail;
}

public void setPatientdetail(Patients patientdetail) {
	this.patientdetail = patientdetail;
}

public String getFilePath() {
	return filePath;
}

public void setFilePath(String filePath) {
	this.filePath = filePath;
}

public ZonedDateTime getAddedTime() {
	return addedTime;
}

public void setAddedTime(ZonedDateTime addedTime) {
	this.addedTime = addedTime;
}

public int getAddedUserid() {
	return addedUserid;
}

public void setAddedUserid(int addedUserid) {
	this.addedUserid = addedUserid;
}

public String getRemark() {
	return remark;
}

public void setRemark(String remark) {
	this.remark = remark;
}

public Appointments getAppointments() {
	return appointments;
}

public void setAppointments(Appointments appointments) {
	this.appointments = appointments;
}

public PatientFiles() {
	super();
	// TODO Auto-generated constructor stub
}


}
