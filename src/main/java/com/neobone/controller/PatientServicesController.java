package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Appointments;
import com.neobone.model.PatientServices;
import com.neobone.model.Patients;
import com.neobone.model.Payments;
import com.neobone.model.Services;
import com.neobone.repository.AppointmentsRepo;
import com.neobone.repository.PatientServicesRepo;
import com.neobone.repository.PatientsRepo;
import com.neobone.repository.PaymentsRepo;
import com.neobone.repository.ServicesRepo;

@Controller
public class PatientServicesController {
@Autowired
PatientServicesRepo patientservicesrepo;

@Autowired
PatientsRepo patientsrepo;

@Autowired
AppointmentsRepo appointmentsrepo;
@Autowired
PaymentsRepo paymentsrepo;
@Autowired
ServicesRepo servicesrepo;

@RequestMapping(value="/patientservice", method=RequestMethod.POST)
@ResponseBody
public PatientServices patientFilesDetails(PatientServices patientservices) {
	Patients getpatientid=patientsrepo.findById(Long.parseLong(patientservices.getPatientServicePatientid())).get();
	Appointments getapptid=appointmentsrepo.findById(Long.parseLong(patientservices.getPatientServiceApptid())).get();
	Services getserviceid=servicesrepo.findById(Long.parseLong(patientservices.getPatientServiceid())).get();
	Payments getpaymentid=paymentsrepo.findById(Long.parseLong(patientservices.getPatientServicePaymentid())).get();
	patientservices.setPatients(getpatientid);
	patientservices.setAppointments(getapptid);
	patientservices.setServices(getserviceid);
	patientservices.setPayments(getpaymentid);
	patientservicesrepo.save(patientservices);

	return patientservices;

}

}
