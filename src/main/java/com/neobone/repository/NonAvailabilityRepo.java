package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.NonAvailability;

public interface NonAvailabilityRepo extends JpaRepository<NonAvailability,Long> {

}
