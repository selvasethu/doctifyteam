package com.neobone.model;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
public class PatientServices {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long patientserviceid;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime serviceDate = ZonedDateTime.now();

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="apptid", referencedColumnName="apptid")
private Appointments appointments;

@Transient
private String patientServiceApptid;



public String getPatientServiceApptid() {
	return patientServiceApptid;
}

public void setPatientServiceApptid(String patientServiceApptid) {
	this.patientServiceApptid = patientServiceApptid;
}

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="patientid", referencedColumnName="patientid")
private Patients patients;

@Transient
private String patientServicePatientid;



public String getPatientServicePatientid() {
	return patientServicePatientid;
}

public void setPatientServicePatientid(String patientServicePatientid) {
	this.patientServicePatientid = patientServicePatientid;
}

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="serviceid", referencedColumnName="serviceid")
private Services services;

@Transient
private String patientServiceid;



public String getPatientServiceid() {
	return patientServiceid;
}

public void setPatientServiceid(String patientServiceid) {
	this.patientServiceid = patientServiceid;
}

@Column
private String remark;
@Column
private int serviceFee;
@Column
private int quantity;

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="paymentid", referencedColumnName="paymentid")
private Payments payments;

@Transient
private String patientServicePaymentid;



public String getPatientServicePaymentid() {
	return patientServicePaymentid;
}

public void setPatientServicePaymentid(String patientServicePaymentid) {
	this.patientServicePaymentid = patientServicePaymentid;
}

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime createdTime = ZonedDateTime.now();

@Column
private int createuserid;

public Long getPatientserviceid() {
	return patientserviceid;
}

public void setPatientserviceid(Long patientserviceid) {
	this.patientserviceid = patientserviceid;
}

public ZonedDateTime getServiceDate() {
	return serviceDate;
}

public void setServiceDate(ZonedDateTime serviceDate) {
	this.serviceDate = serviceDate;
}

public Appointments getAppointments() {
	return appointments;
}

public void setAppointments(Appointments appointments) {
	this.appointments = appointments;
}

public Patients getPatients() {
	return patients;
}

public void setPatients(Patients patients) {
	this.patients = patients;
}

public Services getServices() {
	return services;
}

public void setServices(Services services) {
	this.services = services;
}

public String getRemark() {
	return remark;
}

public void setRemark(String remark) {
	this.remark = remark;
}

public int getServiceFee() {
	return serviceFee;
}

public void setServiceFee(int serviceFee) {
	this.serviceFee = serviceFee;
}

public int getQuantity() {
	return quantity;
}

public void setQuantity(int quantity) {
	this.quantity = quantity;
}

public Payments getPayments() {
	return payments;
}

public void setPayments(Payments payments) {
	this.payments = payments;
}

public ZonedDateTime getCreatedTime() {
	return createdTime;
}

public void setCreatedTime(ZonedDateTime createdTime) {
	this.createdTime = createdTime;
}

public int getCreateuserid() {
	return createuserid;
}

public void setCreateuserid(int createuserid) {
	this.createuserid = createuserid;
}

public PatientServices() {
	super();
	// TODO Auto-generated constructor stub
}
  
}
