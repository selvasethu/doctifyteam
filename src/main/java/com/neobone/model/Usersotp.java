package com.neobone.model;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Table
@Entity
public class Usersotp {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long userOtpid;
@Column
private String mobileNo;

@Column
private int otp;
@Column
private String isActive;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime expiryDate = ZonedDateTime.now();
@Column
private String createdBy;
@Column
private String modifiedBy;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime createdDate = ZonedDateTime.now();

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime modifiedDate = ZonedDateTime.now();

public Long getUserOtpid() {
	return userOtpid;
}

public void setUserOtpid(Long userOtpid) {
	this.userOtpid = userOtpid;
}

public String getMobileNo() {
	return mobileNo;
}

public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
}

public int getOtp() {
	return otp;
}

public void setOtp(int otp) {
	this.otp = otp;
}

public String getIsActive() {
	return isActive;
}

public void setIsActive(String isActive) {
	this.isActive = isActive;
}

public ZonedDateTime getExpiryDate() {
	return expiryDate;
}

public void setExpiryDate(ZonedDateTime expiryDate) {
	this.expiryDate = expiryDate;
}

public String getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}

public String getModifiedBy() {
	return modifiedBy;
}

public void setModifiedBy(String modifiedBy) {
	this.modifiedBy = modifiedBy;
}

public ZonedDateTime getCreatedDate() {
	return createdDate;
}

public void setCreatedDate(ZonedDateTime createdDate) {
	this.createdDate = createdDate;
}

public ZonedDateTime getModifiedDate() {
	return modifiedDate;
}

public void setModifiedDate(ZonedDateTime modifiedDate) {
	this.modifiedDate = modifiedDate;
}

public Usersotp() {
	super();
	// TODO Auto-generated constructor stub
}




}
