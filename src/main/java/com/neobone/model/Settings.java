package com.neobone.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table
@Entity
public class Settings {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long settingsid;

@ManyToOne(cascade = CascadeType.PERSIST)
@JoinColumn(name = "doctorid", referencedColumnName = "userid")
private Users users;

@ManyToOne(cascade = CascadeType.ALL)
@JoinColumn(name = "clinicid", referencedColumnName = "clinicid")
private Clinics clinic;

@Column
private String appointmentLimit;

@Column
private String clinicUrl;
@Column
private String email1;
@Column
private String email2;
@Column
private String email3;
@Column
private String mobile1;
@Column
private String mobile2;
@Column
private String mobile3;
@Column
private String clinicName;
@Column
private String doctorName;
public Long getSettingsid() {
	return settingsid;
}
public void setSettingsid(Long settingsid) {
	this.settingsid = settingsid;
}
public Users getUsers() {
	return users;
}
public void setUsers(Users users) {
	this.users = users;
}
public Clinics getClinic() {
	return clinic;
}
public void setClinic(Clinics clinic) {
	this.clinic = clinic;
}
public String getAppointmentLimit() {
	return appointmentLimit;
}
public void setAppointmentLimit(String appointmentLimit) {
	this.appointmentLimit = appointmentLimit;
}
public String getClinicUrl() {
	return clinicUrl;
}
public void setClinicUrl(String clinicUrl) {
	this.clinicUrl = clinicUrl;
}
public String getEmail1() {
	return email1;
}
public void setEmail1(String email1) {
	this.email1 = email1;
}
public String getEmail2() {
	return email2;
}
public void setEmail2(String email2) {
	this.email2 = email2;
}
public String getEmail3() {
	return email3;
}
public void setEmail3(String email3) {
	this.email3 = email3;
}
public String getMobile1() {
	return mobile1;
}
public void setMobile1(String mobile1) {
	this.mobile1 = mobile1;
}
public String getMobile2() {
	return mobile2;
}
public void setMobile2(String mobile2) {
	this.mobile2 = mobile2;
}
public String getMobile3() {
	return mobile3;
}
public void setMobile3(String mobile3) {
	this.mobile3 = mobile3;
}
public String getClinicName() {
	return clinicName;
}
public void setClinicName(String clinicName) {
	this.clinicName = clinicName;
}
public String getDoctorName() {
	return doctorName;
}
public void setDoctorName(String doctorName) {
	this.doctorName = doctorName;
}
public Settings() {
	super();
	// TODO Auto-generated constructor stub
}


}
