package com.neobone.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table
public class Users {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long userid;

@Column 
private String userType;
@Column
private String name;
@Column
private String gender;
@Column
private String mobile;
@Column
private String email;
@Column
private String userName;
@Column
private String password;
@Column
private String active;

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="specialtyid", referencedColumnName="specialtyid")
private Specialties specialty;
 
@Transient
private String userspecialtyid;



public String getUserspecialtyid() {
	return userspecialtyid;
}
public void setUserspecialtyid(String userspecialtyid) {
	this.userspecialtyid = userspecialtyid;
}

@Column 
private String degrees;

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="clinicid",referencedColumnName="clinicid")
private Clinics clinics;

@Transient
private String userclinicid;



public String getUserclinicid() {
	return userclinicid;
}
public void setUserclinicid(String userclinicid) {
	this.userclinicid = userclinicid;
}

@Column
private String address1;
@Column
private String address2;
@Column
private String city;
public Long getUserid() {
	return userid;
}
public void setUserid(Long userid) {
	this.userid = userid;
}
public String getUserType() {
	return userType;
}
public void setUserType(String userType) {
	this.userType = userType;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getMobile() {
	return mobile;
}
public void setMobile(String mobile) {
	this.mobile = mobile;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getUserName() {
	return userName;
}
public void setUserName(String userName) {
	this.userName = userName;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getActive() {
	return active;
}
public void setActive(String active) {
	this.active = active;
}
public Specialties getSpecialty() {
	return specialty;
}
public void setSpecialty(Specialties specialty) {
	this.specialty = specialty;
}
public String getDegrees() {
	return degrees;
}
public void setDegrees(String degrees) {
	this.degrees = degrees;
}
public Clinics getClinics() {
	return clinics;
}
public void setClinics(Clinics clinics) {
	this.clinics = clinics;
}
public String getAddress1() {
	return address1;
}
public void setAddress1(String address1) {
	this.address1 = address1;
}
public String getAddress2() {
	return address2;
}
public void setAddress2(String address2) {
	this.address2 = address2;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}

public Users() {
	super();
	// TODO Auto-generated constructor stub
}



}
