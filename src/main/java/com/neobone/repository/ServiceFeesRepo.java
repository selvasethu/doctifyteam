package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.ServiceFees;

public interface ServiceFeesRepo extends JpaRepository <ServiceFees,Long> {

}
