package com.neobone.model;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Table
@Entity
public class BlockedSlots {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long blockid;

@ManyToOne(cascade = CascadeType.PERSIST)
@JoinColumn(name = "doctorid", referencedColumnName = "userid")
private Users users;

@Transient
private String blockuserid;



public String getBlockuserid() {
	return blockuserid;
}

public void setBlockuserid(String blockuserid) {
	this.blockuserid = blockuserid;
}

@ManyToOne(cascade = CascadeType.PERSIST)
@JoinColumn(name = "clinicid", referencedColumnName = "clinicid")
private Clinics clinic;

@Transient
private String blockclinicid;



public String getBlockclinicid() {
	return blockclinicid;
}

public void setBlockclinicid(String blockclinicid) {
	this.blockclinicid = blockclinicid;
}

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime date = ZonedDateTime.now();

@Column
private String slot;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime startTime = ZonedDateTime.now();

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime endTime = ZonedDateTime.now();

public Long getBlockid() {
	return blockid;
}

public void setBlockid(Long blockid) {
	this.blockid = blockid;
}

public Users getUsers() {
	return users;
}

public void setUsers(Users users) {
	this.users = users;
}

public Clinics getClinic() {
	return clinic;
}

public void setClinic(Clinics clinic) {
	this.clinic = clinic;
}

public ZonedDateTime getDate() {
	return date;
}

public void setDate(ZonedDateTime date) {
	this.date = date;
}

public String getSlot() {
	return slot;
}

public void setSlot(String slot) {
	this.slot = slot;
}

public ZonedDateTime getStartTime() {
	return startTime;
}

public void setStartTime(ZonedDateTime startTime) {
	this.startTime = startTime;
}

public ZonedDateTime getEndTime() {
	return endTime;
}

public void setEndTime(ZonedDateTime endTime) {
	this.endTime = endTime;
}

public BlockedSlots() {
	super();
	// TODO Auto-generated constructor stub
}



}
