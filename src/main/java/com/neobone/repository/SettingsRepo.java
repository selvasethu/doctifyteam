package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.Settings;

public interface SettingsRepo extends JpaRepository<Settings,Long>  {

}
