package com.neobone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table 
@Entity
public class WebAdmin {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long userid;
@Column
private String userName;
@Column
private String password;

@Column
private String mobile;

@Column
private String email;

@Column
private String superAdmin;

@Column
private String financeData;

public Long getUserid() {
	return userid;
}

public void setUserid(Long userid) {
	this.userid = userid;
}

public String getUserName() {
	return userName;
}

public void setUserName(String userName) {
	this.userName = userName;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getMobile() {
	return mobile;
}

public void setMobile(String mobile) {
	this.mobile = mobile;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getSuperAdmin() {
	return superAdmin;
}

public void setSuperAdmin(String superAdmin) {
	this.superAdmin = superAdmin;
}

public String getFinanceData() {
	return financeData;
}

public void setFinanceData(String financeData) {
	this.financeData = financeData;
}

public WebAdmin() {
	super();
	// TODO Auto-generated constructor stub
}



}
