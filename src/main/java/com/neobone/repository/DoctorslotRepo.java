package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.Doctorslot;

public interface DoctorslotRepo extends JpaRepository<Doctorslot,Long> {

}
