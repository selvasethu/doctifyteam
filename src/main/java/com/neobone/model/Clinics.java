package com.neobone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Clinics {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long clinicid;
@Column
private String clinicName;
@Column
private String locality;
@Column
private String address1;
@Column
private String address2;
@Column
private String city;
@Column
private String phoneNumber;
@Column
private String weburl;
@Column
private String shortcode;


public Long getClinicid() {
	return clinicid;
}
public void setClinicid(Long clinicid) {
	this.clinicid = clinicid;
}
public String getClinicName() {
	return clinicName;
}
public void setClinicName(String clinicName) {
	this.clinicName = clinicName;
}
public String getLocality() {
	return locality;
}
public void setLocality(String locality) {
	this.locality = locality;
}
public String getAddress1() {
	return address1;
}
public void setAddress1(String address1) {
	this.address1 = address1;
}
public String getAddress2() {
	return address2;
}
public void setAddress2(String address2) {
	this.address2 = address2;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getPhoneNumber() {
	return phoneNumber;
}
public void setPhoneNumber(String phoneNumber) {
	this.phoneNumber = phoneNumber;
}
public String getWeburl() {
	return weburl;
}
public void setWeburl(String weburl) {
	this.weburl = weburl;
}
public String getShortcode() {
	return shortcode;
}
public void setShortcode(String shortcode) {
	this.shortcode = shortcode;
}

public Clinics() {
	super();
	// TODO Auto-generated constructor stub
}




}
