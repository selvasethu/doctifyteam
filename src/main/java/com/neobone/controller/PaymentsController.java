package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Clinics;
import com.neobone.model.Patients;
import com.neobone.model.Payments;
import com.neobone.repository.ClinicsRepo;
import com.neobone.repository.PatientsRepo;
import com.neobone.repository.PaymentsRepo;

@Controller
public class PaymentsController {
	@Autowired
	PaymentsRepo paymentsrepo;

	 @Autowired
	  ClinicsRepo clinicsrepo;
	  @Autowired
	  PatientsRepo patientsrepo;
	  
	@RequestMapping(value="/paymentpage", method=RequestMethod.POST)
	public String paymentsDetail(){
		return "payment";
		
	}

	@RequestMapping(value="/payment", method=RequestMethod.POST)
	@ResponseBody
	public Payments paymentsDetails(Payments payments) {
		 Patients getpatentsid=patientsrepo.findById(Long.parseLong(payments.getPaymentpatientid())).get();
		 Clinics getclinicid=clinicsrepo.findById(Long.parseLong(payments.getPaymentclinicid())).get();
		 payments.setPatients(getpatentsid);
		 payments.setClinics(getclinicid);
		paymentsrepo.save(payments);
		return payments;
	}

}
