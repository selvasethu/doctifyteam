package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Appointments;
import com.neobone.model.Patients;
import com.neobone.model.Payments;
import com.neobone.model.Services;
import com.neobone.model.UnpaidServices;
import com.neobone.repository.AppointmentsRepo;
import com.neobone.repository.PatientsRepo;
import com.neobone.repository.PaymentsRepo;
import com.neobone.repository.ServicesRepo;
import com.neobone.repository.UnpaidServicesRepo;

@Controller
public class UnpaidServicesController {
@Autowired
UnpaidServicesRepo unpaidservicesrepo;

@Autowired
PatientsRepo patientsrepo;

@Autowired
AppointmentsRepo appointmentsrepo;
@Autowired
PaymentsRepo paymentsrepo;
@Autowired
ServicesRepo servicesrepo;

@RequestMapping(value="/unpaidservice", method=RequestMethod.POST)
@ResponseBody
public UnpaidServices patientFilesDetails(UnpaidServices unpaidservices) {
	Patients getpatientid=patientsrepo.findById(Long.parseLong(unpaidservices.getUnpaidPatientid())).get();
	Appointments getapptid=appointmentsrepo.findById(Long.parseLong(unpaidservices.getUnpaidApptid())).get();
	Services getserviceid=servicesrepo.findById(Long.parseLong(unpaidservices.getUnpaidserviceid())).get();
	Payments getpaymentid=paymentsrepo.findById(Long.parseLong(unpaidservices.getUnpaidpaymentid())).get();
	unpaidservices.setPatients(getpatientid);
	unpaidservices.setAppointments(getapptid);
	unpaidservices.setServices(getserviceid);
	unpaidservices.setPayments(getpaymentid);
	unpaidservicesrepo.save(unpaidservices);

	return unpaidservices;

}


}
