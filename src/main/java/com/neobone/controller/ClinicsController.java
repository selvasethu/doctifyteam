package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Clinics;
import com.neobone.repository.ClinicsRepo;


@Controller
public class ClinicsController {
	@Autowired
	ClinicsRepo clinicsrepo;

	@RequestMapping(value="/clinicspage", method=RequestMethod.POST)
	public String clinics(){
		return "clinics";
		
	}

	@RequestMapping(value="/clinics", method=RequestMethod.POST)
	@ResponseBody
	public String clinicsDetails(Clinics clinics) {
		
		clinicsrepo.save(clinics);
		return "clinics";
	}

}
