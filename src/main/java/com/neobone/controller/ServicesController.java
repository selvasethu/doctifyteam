package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Services;
import com.neobone.repository.ServicesRepo;

@Controller
public class ServicesController {
@Autowired
ServicesRepo servicesrepo;

@RequestMapping(value="/services", method=RequestMethod.POST)
@ResponseBody
public String servicesDetails(Services services) {
	
	servicesrepo.save(services);
	return "services";
}

}
