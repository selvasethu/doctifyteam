package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.PatientServices;

public interface PatientServicesRepo extends JpaRepository<PatientServices,Long> {

}
