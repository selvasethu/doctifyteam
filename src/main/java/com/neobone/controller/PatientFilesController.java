package com.neobone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Appointments;
import com.neobone.model.PatientFiles;
import com.neobone.model.Patients;
import com.neobone.repository.AppointmentsRepo;
import com.neobone.repository.PatientFilesRepo;
import com.neobone.repository.PatientsRepo;

@Controller
public class PatientFilesController {
@Autowired
PatientFilesRepo patientfilesrepo;

@Autowired
PatientsRepo patientsrepo;

@Autowired
AppointmentsRepo appointmentsrepo;

@RequestMapping(value="/patientfiles", method=RequestMethod.POST)
@ResponseBody
public PatientFiles patientFilesDetails(PatientFiles patientfiles) {
	Patients getpatientid=patientsrepo.findById(Long.parseLong(patientfiles.getFilePatientid())).get();
	Appointments getapptid=appointmentsrepo.findById(Long.parseLong(patientfiles.getFileApptid())).get();
	patientfiles.setPatientdetail(getpatientid);
	patientfiles.setAppointments(getapptid);
		/*
		 * Payments getpaymentid=paymentsrepo.findById(Long.parseLong(appointmentsmodel.
		 * getAppoinmentpaymentid())).get();
		 * appointmentsmodel.setPayments(getpaymentid);
		 */
	patientfilesrepo.save(patientfiles);

	return patientfiles;

}

@RequestMapping(value="/findalpatientfiles", method=RequestMethod.POST)
@ResponseBody
public List<PatientFiles> findallpatientFilesDetails(PatientFiles patientfiles) {
	List<PatientFiles> des=patientfilesrepo.findAll();


		/*
		 * Payments getpaymentid=paymentsrepo.findById(Long.parseLong(appointmentsmodel.
		 * getAppoinmentpaymentid())).get();
		 * appointmentsmodel.setPayments(getpaymentid);
		 */

	return des;
}
}
