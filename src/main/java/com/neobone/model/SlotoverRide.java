package com.neobone.model;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
public class SlotoverRide {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long splSlotid;

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="doctorid", referencedColumnName="userid")
private Users users;

@Transient
private String overRideUserid;



public String getOverRideUserid() {
	return overRideUserid;
}

public void setOverRideUserid(String overRideUserid) {
	this.overRideUserid = overRideUserid;
}



@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="clinicid", referencedColumnName="clinicid")
private Clinics clinics;

@Transient
private String overRideClinicid;



public String getOverRideClinicid() {
	return overRideClinicid;
}

public void setOverRideClinicid(String overRideClinicid) {
	this.overRideClinicid = overRideClinicid;
}



@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime date = ZonedDateTime.now();
 
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime start1 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime end1 = ZonedDateTime.now();

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime start2 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime end2 = ZonedDateTime.now();

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime start3 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime end3 = ZonedDateTime.now();

@Column
private int consultationFee;

@Column
private Byte minutesPerSlot;

@Column
private int firstBlock1;

@Column
private int lastBlock1;
@Column
private int firstBlock2;

@Column
private int lastBlock2;
@Column
private int firstBlock3;

@Column
private int lastBlock3;

public Long getSplSlotid() {
	return splSlotid;
}

public void setSplSlotid(Long splSlotid) {
	this.splSlotid = splSlotid;
}

public Users getUsers() {
	return users;
}

public void setUsers(Users users) {
	this.users = users;
}

public Clinics getClinics() {
	return clinics;
}

public void setClinics(Clinics clinics) {
	this.clinics = clinics;
}

public ZonedDateTime getDate() {
	return date;
}

public void setDate(ZonedDateTime date) {
	this.date = date;
}

public ZonedDateTime getStart1() {
	return start1;
}

public void setStart1(ZonedDateTime start1) {
	this.start1 = start1;
}

public ZonedDateTime getEnd1() {
	return end1;
}

public void setEnd1(ZonedDateTime end1) {
	this.end1 = end1;
}

public ZonedDateTime getStart2() {
	return start2;
}

public void setStart2(ZonedDateTime start2) {
	this.start2 = start2;
}

public ZonedDateTime getEnd2() {
	return end2;
}

public void setEnd2(ZonedDateTime end2) {
	this.end2 = end2;
}

public ZonedDateTime getStart3() {
	return start3;
}

public void setStart3(ZonedDateTime start3) {
	this.start3 = start3;
}

public ZonedDateTime getEnd3() {
	return end3;
}

public void setEnd3(ZonedDateTime end3) {
	this.end3 = end3;
}

public int getConsultationFee() {
	return consultationFee;
}

public void setConsultationFee(int consultationFee) {
	this.consultationFee = consultationFee;
}

public Byte getMinutesPerSlot() {
	return minutesPerSlot;
}

public void setMinutesPerSlot(Byte minutesPerSlot) {
	this.minutesPerSlot = minutesPerSlot;
}

public int getFirstBlock1() {
	return firstBlock1;
}

public void setFirstBlock1(int firstBlock1) {
	this.firstBlock1 = firstBlock1;
}

public int getLastBlock1() {
	return lastBlock1;
}

public void setLastBlock1(int lastBlock1) {
	this.lastBlock1 = lastBlock1;
}

public int getFirstBlock2() {
	return firstBlock2;
}

public void setFirstBlock2(int firstBlock2) {
	this.firstBlock2 = firstBlock2;
}

public int getLastBlock2() {
	return lastBlock2;
}

public void setLastBlock2(int lastBlock2) {
	this.lastBlock2 = lastBlock2;
}

public int getFirstBlock3() {
	return firstBlock3;
}

public void setFirstBlock3(int firstBlock3) {
	this.firstBlock3 = firstBlock3;
}

public int getLastBlock3() {
	return lastBlock3;
}

public void setLastBlock3(int lastBlock3) {
	this.lastBlock3 = lastBlock3;
}



public SlotoverRide() {
	super();
	// TODO Auto-generated constructor stub
}

 
}
