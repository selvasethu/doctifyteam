
  package com.neobone.repository;
 
  import org.springframework.data.jpa.repository.JpaRepository;
  import org.springframework.stereotype.Repository;

import com.neobone.model.Appointments;
  
  @Repository 
  public interface AppointmentsRepo extends JpaRepository<Appointments,Long> {
 
 }
