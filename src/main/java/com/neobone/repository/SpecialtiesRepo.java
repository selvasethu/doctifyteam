package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.Specialties;

public interface SpecialtiesRepo extends JpaRepository <Specialties,Long>{

}
