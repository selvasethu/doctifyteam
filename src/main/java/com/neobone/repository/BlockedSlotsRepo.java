package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.BlockedSlots;

public interface BlockedSlotsRepo extends JpaRepository <BlockedSlots,Long> {

}
