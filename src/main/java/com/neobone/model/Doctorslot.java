package com.neobone.model;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
public class Doctorslot {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long slotid;
@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="doctorid", referencedColumnName="userid")
private Users users;

@Transient
private String slotuserid;



public String getSlotuserid() {
	return slotuserid;
}
public void setSlotuserid(String slotuserid) {
	this.slotuserid = slotuserid;
}

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="clinicid", referencedColumnName="clinicid")
private Clinics clinics;

@Transient
private String slotclinicid;



public String getSlotclinicid() {
	return slotclinicid;
}
public void setSlotclinicid(String slotclinicid) {
	this.slotclinicid = slotclinicid;
}

@Column
private int consultationFee;

@Column
private Byte minutesPerSlot;

@Column
private int monday;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime monStart1 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime monEnd1 = ZonedDateTime.now();

@Column
private int monFirstBlock1;

@Column
private int  monLastBlock1;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime monStart2 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime monEnd2 = ZonedDateTime.now();

@Column
private int monFirstBlock2;
@Column
private int monLastBlock2;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime monStart3 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime monEnd3 = ZonedDateTime.now();
@Column
private int monFirstBlock3;
@Column
private int monLastBlock3;

@Column
private int tuesday;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime tueStart1 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime tueEnd1 = ZonedDateTime.now();

@Column
private int tueFirstBlock1;

@Column
private int  tueLastBlock1;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime tueStart2 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime tueEnd2 = ZonedDateTime.now();

@Column
private int tueFirstBlock2;
@Column
private int tueLastBlock2;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime tueStart3 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime tueEnd3 = ZonedDateTime.now();
@Column
private int tueFirstBlock3;
@Column
private int tueLastBlock3;

@Column
private int wednesday;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime wedStart1 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime wedEnd1 = ZonedDateTime.now();
@Column
private int wedFirstBlock1;

@Column
private int  wedLastBlock1;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime wedStart2 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime wedEnd2 = ZonedDateTime.now();
@Column
private int wedFirstBlock2;
@Column
private int wedLastBlock2;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime wedStart3 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime wedEnd3 = ZonedDateTime.now();
@Column
private int wedFirstBlock3;
@Column
private int wedLastBlock3;

@Column
private int thursday;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime thuStart1 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime thuEnd1 = ZonedDateTime.now();
@Column
private int thuFirstBlock1;

@Column
private int  thuLastBlock1;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime thuStart2 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime thuEnd2 = ZonedDateTime.now();
@Column
private int thuFirstBlock2;
@Column
private int thuLastBlock2;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime thuStart3 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime thuEnd3 = ZonedDateTime.now();
@Column
private int thuFirstBlock3;
@Column
private int thuLastBlock3;

@Column
private int friday;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime friStart1 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime friEnd1 = ZonedDateTime.now();
@Column
private int friFirstBlock1;

@Column
private int  friLastBlock1;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime friStart2 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime friEnd2 = ZonedDateTime.now();
@Column
private int friFirstBlock2;
@Column
private int friLastBlock2;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime friStart3 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime friEnd3 = ZonedDateTime.now();
@Column
private int friFirstBlock3;
@Column
private int friLastBlock3;

@Column
private int saturday;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime satStart1 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime satEnd1 = ZonedDateTime.now();
@Column
private int satFirstBlock1;

@Column
private int  satLastBlock1;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime satStart2 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime satEnd2 = ZonedDateTime.now();
@Column
private int satFirstBlock2;
@Column
private int satLastBlock2;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime satStart3 = ZonedDateTime.now();
@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime satEnd3 = ZonedDateTime.now();
@Column
private int satFirstBlock3;
@Column
private int satLastBlock3;
public Long getSlotid() {
	return slotid;
}
public void setSlotid(Long slotid) {
	this.slotid = slotid;
}
public Users getUsers() {
	return users;
}
public void setUsers(Users users) {
	this.users = users;
}
public Clinics getClinics() {
	return clinics;
}
public void setClinics(Clinics clinics) {
	this.clinics = clinics;
}
public int getConsultationFee() {
	return consultationFee;
}
public void setConsultationFee(int consultationFee) {
	this.consultationFee = consultationFee;
}
public Byte getMinutesPerSlot() {
	return minutesPerSlot;
}
public void setMinutesPerSlot(Byte minutesPerSlot) {
	this.minutesPerSlot = minutesPerSlot;
}
public int getMonday() {
	return monday;
}
public void setMonday(int monday) {
	this.monday = monday;
}
public ZonedDateTime getMonStart1() {
	return monStart1;
}
public void setMonStart1(ZonedDateTime monStart1) {
	this.monStart1 = monStart1;
}
public ZonedDateTime getMonEnd1() {
	return monEnd1;
}
public void setMonEnd1(ZonedDateTime monEnd1) {
	this.monEnd1 = monEnd1;
}
public int getMonFirstBlock1() {
	return monFirstBlock1;
}
public void setMonFirstBlock1(int monFirstBlock1) {
	this.monFirstBlock1 = monFirstBlock1;
}
public int getMonLastBlock1() {
	return monLastBlock1;
}
public void setMonLastBlock1(int monLastBlock1) {
	this.monLastBlock1 = monLastBlock1;
}
public ZonedDateTime getMonStart2() {
	return monStart2;
}
public void setMonStart2(ZonedDateTime monStart2) {
	this.monStart2 = monStart2;
}
public ZonedDateTime getMonEnd2() {
	return monEnd2;
}
public void setMonEnd2(ZonedDateTime monEnd2) {
	this.monEnd2 = monEnd2;
}
public int getMonFirstBlock2() {
	return monFirstBlock2;
}
public void setMonFirstBlock2(int monFirstBlock2) {
	this.monFirstBlock2 = monFirstBlock2;
}
public int getMonLastBlock2() {
	return monLastBlock2;
}
public void setMonLastBlock2(int monLastBlock2) {
	this.monLastBlock2 = monLastBlock2;
}
public ZonedDateTime getMonStart3() {
	return monStart3;
}
public void setMonStart3(ZonedDateTime monStart3) {
	this.monStart3 = monStart3;
}
public ZonedDateTime getMonEnd3() {
	return monEnd3;
}
public void setMonEnd3(ZonedDateTime monEnd3) {
	this.monEnd3 = monEnd3;
}
public int getMonFirstBlock3() {
	return monFirstBlock3;
}
public void setMonFirstBlock3(int monFirstBlock3) {
	this.monFirstBlock3 = monFirstBlock3;
}
public int getMonLastBlock3() {
	return monLastBlock3;
}
public void setMonLastBlock3(int monLastBlock3) {
	this.monLastBlock3 = monLastBlock3;
}
public int getTuesday() {
	return tuesday;
}
public void setTuesday(int tuesday) {
	this.tuesday = tuesday;
}
public ZonedDateTime getTueStart1() {
	return tueStart1;
}
public void setTueStart1(ZonedDateTime tueStart1) {
	this.tueStart1 = tueStart1;
}
public ZonedDateTime getTueEnd1() {
	return tueEnd1;
}
public void setTueEnd1(ZonedDateTime tueEnd1) {
	this.tueEnd1 = tueEnd1;
}
public int getTueFirstBlock1() {
	return tueFirstBlock1;
}
public void setTueFirstBlock1(int tueFirstBlock1) {
	this.tueFirstBlock1 = tueFirstBlock1;
}
public int getTueLastBlock1() {
	return tueLastBlock1;
}
public void setTueLastBlock1(int tueLastBlock1) {
	this.tueLastBlock1 = tueLastBlock1;
}
public ZonedDateTime getTueStart2() {
	return tueStart2;
}
public void setTueStart2(ZonedDateTime tueStart2) {
	this.tueStart2 = tueStart2;
}
public ZonedDateTime getTueEnd2() {
	return tueEnd2;
}
public void setTueEnd2(ZonedDateTime tueEnd2) {
	this.tueEnd2 = tueEnd2;
}
public int getTueFirstBlock2() {
	return tueFirstBlock2;
}
public void setTueFirstBlock2(int tueFirstBlock2) {
	this.tueFirstBlock2 = tueFirstBlock2;
}
public int getTueLastBlock2() {
	return tueLastBlock2;
}
public void setTueLastBlock2(int tueLastBlock2) {
	this.tueLastBlock2 = tueLastBlock2;
}
public ZonedDateTime getTueStart3() {
	return tueStart3;
}
public void setTueStart3(ZonedDateTime tueStart3) {
	this.tueStart3 = tueStart3;
}
public ZonedDateTime getTueEnd3() {
	return tueEnd3;
}
public void setTueEnd3(ZonedDateTime tueEnd3) {
	this.tueEnd3 = tueEnd3;
}
public int getTueFirstBlock3() {
	return tueFirstBlock3;
}
public void setTueFirstBlock3(int tueFirstBlock3) {
	this.tueFirstBlock3 = tueFirstBlock3;
}
public int getTueLastBlock3() {
	return tueLastBlock3;
}
public void setTueLastBlock3(int tueLastBlock3) {
	this.tueLastBlock3 = tueLastBlock3;
}
public int getWednesday() {
	return wednesday;
}
public void setWednesday(int wednesday) {
	this.wednesday = wednesday;
}
public ZonedDateTime getWedStart1() {
	return wedStart1;
}
public void setWedStart1(ZonedDateTime wedStart1) {
	this.wedStart1 = wedStart1;
}
public ZonedDateTime getWedEnd1() {
	return wedEnd1;
}
public void setWedEnd1(ZonedDateTime wedEnd1) {
	this.wedEnd1 = wedEnd1;
}
public int getWedFirstBlock1() {
	return wedFirstBlock1;
}
public void setWedFirstBlock1(int wedFirstBlock1) {
	this.wedFirstBlock1 = wedFirstBlock1;
}
public int getWedLastBlock1() {
	return wedLastBlock1;
}
public void setWedLastBlock1(int wedLastBlock1) {
	this.wedLastBlock1 = wedLastBlock1;
}
public ZonedDateTime getWedStart2() {
	return wedStart2;
}
public void setWedStart2(ZonedDateTime wedStart2) {
	this.wedStart2 = wedStart2;
}
public ZonedDateTime getWedEnd2() {
	return wedEnd2;
}
public void setWedEnd2(ZonedDateTime wedEnd2) {
	this.wedEnd2 = wedEnd2;
}
public int getWedFirstBlock2() {
	return wedFirstBlock2;
}
public void setWedFirstBlock2(int wedFirstBlock2) {
	this.wedFirstBlock2 = wedFirstBlock2;
}
public int getWedLastBlock2() {
	return wedLastBlock2;
}
public void setWedLastBlock2(int wedLastBlock2) {
	this.wedLastBlock2 = wedLastBlock2;
}
public ZonedDateTime getWedStart3() {
	return wedStart3;
}
public void setWedStart3(ZonedDateTime wedStart3) {
	this.wedStart3 = wedStart3;
}
public ZonedDateTime getWedEnd3() {
	return wedEnd3;
}
public void setWedEnd3(ZonedDateTime wedEnd3) {
	this.wedEnd3 = wedEnd3;
}
public int getWedFirstBlock3() {
	return wedFirstBlock3;
}
public void setWedFirstBlock3(int wedFirstBlock3) {
	this.wedFirstBlock3 = wedFirstBlock3;
}
public int getWedLastBlock3() {
	return wedLastBlock3;
}
public void setWedLastBlock3(int wedLastBlock3) {
	this.wedLastBlock3 = wedLastBlock3;
}
public int getThursday() {
	return thursday;
}
public void setThursday(int thursday) {
	this.thursday = thursday;
}
public ZonedDateTime getThuStart1() {
	return thuStart1;
}
public void setThuStart1(ZonedDateTime thuStart1) {
	this.thuStart1 = thuStart1;
}
public ZonedDateTime getThuEnd1() {
	return thuEnd1;
}
public void setThuEnd1(ZonedDateTime thuEnd1) {
	this.thuEnd1 = thuEnd1;
}
public int getThuFirstBlock1() {
	return thuFirstBlock1;
}
public void setThuFirstBlock1(int thuFirstBlock1) {
	this.thuFirstBlock1 = thuFirstBlock1;
}
public int getThuLastBlock1() {
	return thuLastBlock1;
}
public void setThuLastBlock1(int thuLastBlock1) {
	this.thuLastBlock1 = thuLastBlock1;
}
public ZonedDateTime getThuStart2() {
	return thuStart2;
}
public void setThuStart2(ZonedDateTime thuStart2) {
	this.thuStart2 = thuStart2;
}
public ZonedDateTime getThuEnd2() {
	return thuEnd2;
}
public void setThuEnd2(ZonedDateTime thuEnd2) {
	this.thuEnd2 = thuEnd2;
}
public int getThuFirstBlock2() {
	return thuFirstBlock2;
}
public void setThuFirstBlock2(int thuFirstBlock2) {
	this.thuFirstBlock2 = thuFirstBlock2;
}
public int getThuLastBlock2() {
	return thuLastBlock2;
}
public void setThuLastBlock2(int thuLastBlock2) {
	this.thuLastBlock2 = thuLastBlock2;
}
public ZonedDateTime getThuStart3() {
	return thuStart3;
}
public void setThuStart3(ZonedDateTime thuStart3) {
	this.thuStart3 = thuStart3;
}
public ZonedDateTime getThuEnd3() {
	return thuEnd3;
}
public void setThuEnd3(ZonedDateTime thuEnd3) {
	this.thuEnd3 = thuEnd3;
}
public int getThuFirstBlock3() {
	return thuFirstBlock3;
}
public void setThuFirstBlock3(int thuFirstBlock3) {
	this.thuFirstBlock3 = thuFirstBlock3;
}
public int getThuLastBlock3() {
	return thuLastBlock3;
}
public void setThuLastBlock3(int thuLastBlock3) {
	this.thuLastBlock3 = thuLastBlock3;
}
public int getFriday() {
	return friday;
}
public void setFriday(int friday) {
	this.friday = friday;
}
public ZonedDateTime getFriStart1() {
	return friStart1;
}
public void setFriStart1(ZonedDateTime friStart1) {
	this.friStart1 = friStart1;
}
public ZonedDateTime getFriEnd1() {
	return friEnd1;
}
public void setFriEnd1(ZonedDateTime friEnd1) {
	this.friEnd1 = friEnd1;
}
public int getFriFirstBlock1() {
	return friFirstBlock1;
}
public void setFriFirstBlock1(int friFirstBlock1) {
	this.friFirstBlock1 = friFirstBlock1;
}
public int getFriLastBlock1() {
	return friLastBlock1;
}
public void setFriLastBlock1(int friLastBlock1) {
	this.friLastBlock1 = friLastBlock1;
}
public ZonedDateTime getFriStart2() {
	return friStart2;
}
public void setFriStart2(ZonedDateTime friStart2) {
	this.friStart2 = friStart2;
}
public ZonedDateTime getFriEnd2() {
	return friEnd2;
}
public void setFriEnd2(ZonedDateTime friEnd2) {
	this.friEnd2 = friEnd2;
}
public int getFriFirstBlock2() {
	return friFirstBlock2;
}
public void setFriFirstBlock2(int friFirstBlock2) {
	this.friFirstBlock2 = friFirstBlock2;
}
public int getFriLastBlock2() {
	return friLastBlock2;
}
public void setFriLastBlock2(int friLastBlock2) {
	this.friLastBlock2 = friLastBlock2;
}
public ZonedDateTime getFriStart3() {
	return friStart3;
}
public void setFriStart3(ZonedDateTime friStart3) {
	this.friStart3 = friStart3;
}
public ZonedDateTime getFriEnd3() {
	return friEnd3;
}
public void setFriEnd3(ZonedDateTime friEnd3) {
	this.friEnd3 = friEnd3;
}
public int getFriFirstBlock3() {
	return friFirstBlock3;
}
public void setFriFirstBlock3(int friFirstBlock3) {
	this.friFirstBlock3 = friFirstBlock3;
}
public int getFriLastBlock3() {
	return friLastBlock3;
}
public void setFriLastBlock3(int friLastBlock3) {
	this.friLastBlock3 = friLastBlock3;
}
public int getSaturday() {
	return saturday;
}
public void setSaturday(int saturday) {
	this.saturday = saturday;
}
public ZonedDateTime getSatStart1() {
	return satStart1;
}
public void setSatStart1(ZonedDateTime satStart1) {
	this.satStart1 = satStart1;
}
public ZonedDateTime getSatEnd1() {
	return satEnd1;
}
public void setSatEnd1(ZonedDateTime satEnd1) {
	this.satEnd1 = satEnd1;
}
public int getSatFirstBlock1() {
	return satFirstBlock1;
}
public void setSatFirstBlock1(int satFirstBlock1) {
	this.satFirstBlock1 = satFirstBlock1;
}
public int getSatLastBlock1() {
	return satLastBlock1;
}
public void setSatLastBlock1(int satLastBlock1) {
	this.satLastBlock1 = satLastBlock1;
}
public ZonedDateTime getSatStart2() {
	return satStart2;
}
public void setSatStart2(ZonedDateTime satStart2) {
	this.satStart2 = satStart2;
}
public ZonedDateTime getSatEnd2() {
	return satEnd2;
}
public void setSatEnd2(ZonedDateTime satEnd2) {
	this.satEnd2 = satEnd2;
}
public int getSatFirstBlock2() {
	return satFirstBlock2;
}
public void setSatFirstBlock2(int satFirstBlock2) {
	this.satFirstBlock2 = satFirstBlock2;
}
public int getSatLastBlock2() {
	return satLastBlock2;
}
public void setSatLastBlock2(int satLastBlock2) {
	this.satLastBlock2 = satLastBlock2;
}
public ZonedDateTime getSatStart3() {
	return satStart3;
}
public void setSatStart3(ZonedDateTime satStart3) {
	this.satStart3 = satStart3;
}
public ZonedDateTime getSatEnd3() {
	return satEnd3;
}
public void setSatEnd3(ZonedDateTime satEnd3) {
	this.satEnd3 = satEnd3;
}
public int getSatFirstBlock3() {
	return satFirstBlock3;
}
public void setSatFirstBlock3(int satFirstBlock3) {
	this.satFirstBlock3 = satFirstBlock3;
}
public int getSatLastBlock3() {
	return satLastBlock3;
}
public void setSatLastBlock3(int satLastBlock3) {
	this.satLastBlock3 = satLastBlock3;
}

public Doctorslot() {
	super();
	// TODO Auto-generated constructor stub
}


}
