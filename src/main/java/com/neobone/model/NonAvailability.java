package com.neobone.model;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
public class NonAvailability {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long nonAvailabilityid;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime date = ZonedDateTime.now();
 
@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="doctorid", referencedColumnName="userid")
private Users users;

@Transient
private String nonAvailabilityUserid;



public String getNonAvailabilityUserid() {
	return nonAvailabilityUserid;
}
public void setNonAvailabilityUserid(String nonAvailabilityUserid) {
	this.nonAvailabilityUserid = nonAvailabilityUserid;
}

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="clinicid", referencedColumnName="clinicid")
private Clinics clinics;

@Transient
private String nonAvailabilityClinicid;



public String getNonAvailabilityClinicid() {
	return nonAvailabilityClinicid;
}
public void setNonAvailabilityClinicid(String nonAvailabilityClinicid) {
	this.nonAvailabilityClinicid = nonAvailabilityClinicid;
}

@Column
private int slot1;
@Column
private int slot2;

@Column
private int slot3;
@Column
private String reason;


public Long getNonAvailabilityid() {
	return nonAvailabilityid;
}
public void setNonAvailabilityid(Long nonAvailabilityid) {
	this.nonAvailabilityid = nonAvailabilityid;
}
public ZonedDateTime getDate() {
	return date;
}
public void setDate(ZonedDateTime date) {
	this.date = date;
}
public Clinics getClinics() {
	return clinics;
}
public void setClinics(Clinics clinics) {
	this.clinics = clinics;
}
public Users getUsers() {
	return users;
}
public void setUsers(Users users) {
	this.users = users;
}
public int getSlot1() {
	return slot1;
}
public void setSlot1(int slot1) {
	this.slot1 = slot1;
}
public int getSlot2() {
	return slot2;
}
public void setSlot2(int slot2) {
	this.slot2 = slot2;
}
public int getSlot3() {
	return slot3;
}
public void setSlot3(int slot3) {
	this.slot3 = slot3;
}
public String getReason() {
	return reason;
}
public void setReason(String reason) {
	this.reason = reason;
}

public NonAvailability() {
	super();
	// TODO Auto-generated constructor stub
}


}
