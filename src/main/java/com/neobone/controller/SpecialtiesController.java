package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.neobone.model.Specialties;
import com.neobone.repository.SpecialtiesRepo;

@Controller
public class SpecialtiesController {
	@Autowired
	SpecialtiesRepo specialtiesrepo;

	@RequestMapping(value="/specialtiespage", method=RequestMethod.POST)
	public String specialtiesDetail(){
		return "specialties";
		
	}

	@RequestMapping(value="/specialties", method=RequestMethod.POST)
	@ResponseBody
	public String specialtiesDetails(Specialties specialties) {
		
		specialtiesrepo.save(specialties);
		return "specialties";
	}
}
