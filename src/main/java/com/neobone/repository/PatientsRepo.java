package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neobone.model.Patients;

@Repository
public interface PatientsRepo extends JpaRepository<Patients,Long> {

}
