package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.Clinics;

public interface ClinicsRepo extends JpaRepository<Clinics,Long> {

}
