package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.Services;

public interface ServicesRepo extends JpaRepository<Services,Long> {

}
