package com.neobone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NeoboneApplication {

	public static void main(String[] args) {
		SpringApplication.run(NeoboneApplication.class, args);
	}

}
