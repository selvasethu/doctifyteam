package com.neobone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Table
@Entity

public class Services {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long serviceid;
@Column
private String serviceName;
public Long getServiceid() {
	return serviceid;
}
public void setServiceid(Long serviceid) {
	this.serviceid = serviceid;
}
public String getServiceName() {
	return serviceName;
}
public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}
public Services() {
	super();
	// TODO Auto-generated constructor stub
}


}
