 package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Appointments;
import com.neobone.model.Clinics;
import com.neobone.model.Patients;
import com.neobone.model.Payments;
import com.neobone.model.Users;
import com.neobone.repository.AppointmentsRepo;
import com.neobone.repository.ClinicsRepo;
import com.neobone.repository.PatientsRepo;
import com.neobone.repository.PaymentsRepo;
import com.neobone.repository.UsersRepo;

@Controller
  public class AppointmentsController {
  @Autowired
  AppointmentsRepo appointmentsrepo;
  @Autowired
  UsersRepo usersrepo;
  @Autowired
  PaymentsRepo paymentsrepo;
  @Autowired
  ClinicsRepo clinicsrepo;
  @Autowired
  PatientsRepo patientsrepo;
  
  
  
  @RequestMapping(value="/appointmentspage", method=RequestMethod.POST)
	public String appointments(){
		return "appointments";
  }
  @RequestMapping(value="/appointments", method=RequestMethod.POST)
  @ResponseBody
  public Appointments appointmentsDetails(Appointments appointmentsmodel) {
	  Payments getpaymentid=paymentsrepo.findById(Long.parseLong(appointmentsmodel.getAppointmentpaymentid())).get();
	  System.out.println(getpaymentid);
	 Users getappoinmentuserid=usersrepo.findById(Long.parseLong(appointmentsmodel.getAppointmentuserid())).get();
	 Patients getpatentsid=patientsrepo.findById(Long.parseLong(appointmentsmodel.getAppointmentpatientid())).get();
	 Clinics getclinicid=clinicsrepo.findById(Long.parseLong(appointmentsmodel.getAppointmentclinicid())).get();
	 appointmentsmodel.setPayments(getpaymentid);
	 appointmentsmodel.setUsers(getappoinmentuserid);
	 appointmentsmodel.setPatientdetail(getpatentsid);
	 appointmentsmodel.setClinic(getclinicid);
	  appointmentsrepo.save(appointmentsmodel);
	  
  	return appointmentsmodel;
  
 }
}
 