package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.PatientFiles;

public interface PatientFilesRepo extends JpaRepository <PatientFiles,Long> {

}
