package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.SlotoverRide;

public interface SlotoverRideRepo extends JpaRepository<SlotoverRide,Long> {

}
