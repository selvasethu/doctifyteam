package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.UnpaidServices;

public interface UnpaidServicesRepo extends JpaRepository<UnpaidServices,Long> {

}
