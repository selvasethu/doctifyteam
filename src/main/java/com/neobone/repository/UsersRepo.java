package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.Users;

public interface UsersRepo extends JpaRepository<Users,Long> {

}
