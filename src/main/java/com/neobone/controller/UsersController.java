package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Clinics;
import com.neobone.model.Specialties;
import com.neobone.model.Users;
import com.neobone.repository.ClinicsRepo;
import com.neobone.repository.SpecialtiesRepo;
import com.neobone.repository.UsersRepo;
@Controller
public class UsersController {
	@Autowired
	UsersRepo usersrepo;
@Autowired
SpecialtiesRepo specialtiesrepo;
@Autowired
ClinicsRepo clinicsrepo;


	@RequestMapping(value="/userspage", method=RequestMethod.POST)
	public String UsersDetail(){
		return "users";
		
	}

	@RequestMapping(value="/users", method=RequestMethod.POST)
	@ResponseBody
	public Users usersDetails(Users users) {
		Specialties getuserid=specialtiesrepo.findById(Long.parseLong(users.getUserspecialtyid())).get();
		 Clinics getclinicid=clinicsrepo.findById(Long.parseLong(users.getUserclinicid())).get();
		 users.setSpecialty(getuserid);
		 users.setClinics(getclinicid);
		usersrepo.save(users);
		return users;
		
	}

}
