package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Clinics;
import com.neobone.model.NonAvailability;
import com.neobone.model.Users;
import com.neobone.repository.ClinicsRepo;
import com.neobone.repository.NonAvailabilityRepo;
import com.neobone.repository.UsersRepo;

@Controller
public class NonAvailabilityController {
@Autowired
NonAvailabilityRepo nonavailabilityrepo;

@Autowired
UsersRepo usersrepo;

@Autowired
ClinicsRepo clinicsrepo;

@RequestMapping(value="/nonavailabilitypage", method=RequestMethod.POST)
public String nonavailabilityDetail(){
	return "nonavailability";
	
}

@RequestMapping(value="/nonavailability", method=RequestMethod.POST)
@ResponseBody
public NonAvailability nonavailabilityDetails(NonAvailability nonavailability) {
	Users getuserid=usersrepo.findById(Long.parseLong(nonavailability.getNonAvailabilityUserid())).get();
	 Clinics getclinicid=clinicsrepo.findById(Long.parseLong(nonavailability.getNonAvailabilityClinicid())).get();
	 nonavailability.setUsers(getuserid);
	 nonavailability.setClinics(getclinicid);
	nonavailabilityrepo.save(nonavailability);
	return nonavailability;
}
}
