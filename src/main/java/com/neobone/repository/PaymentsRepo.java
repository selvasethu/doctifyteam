package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.neobone.model.Payments;
@Repository
public interface PaymentsRepo extends JpaRepository<Payments, Long>{

}
