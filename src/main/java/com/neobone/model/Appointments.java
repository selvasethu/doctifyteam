
package com.neobone.model;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Table
@Entity
public class Appointments {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private Long apptid;

	@Column(nullable = false)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime apptDate = ZonedDateTime.now();

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "patientsid", referencedColumnName = "patientid")
	private Patients patientdetail;

	
	  @Transient 
	  private String appointmentpatientid;
	  
	/*
	 * public Patients getpatientDetail() { return patientdetail; }
	 */
	 

	public String getAppointmentpatientid() {
		return appointmentpatientid;
	}

	public void setAppointmentpatientid(String appointmentpatientid) {
		this.appointmentpatientid = appointmentpatientid;
	}







	@Column
	private String remark;
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "doctorid", referencedColumnName = "userid")
	private Users users;

	
	  @Transient
	  private String appointmentuserid;
	 

	public String getAppointmentuserid() {
		return appointmentuserid;
	}

	public void setAppointmentuserid(String appointmentuserid) {
		this.appointmentuserid = appointmentuserid;
	}







	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "clinicid", referencedColumnName = "clinicid")
	private Clinics clinic;

	
	@Transient
	 private String appointmentclinicid;
	
   public String getAppointmentclinicid() {
		return appointmentclinicid;
	}

	public void setAppointmentclinicid(String appointmentclinicid) {
		this.appointmentclinicid = appointmentclinicid;
	}



	 

	@Column
	private String slot;

	@Column(nullable = false)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime startTime = ZonedDateTime.now();

	@Column(nullable = false)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime endTime = ZonedDateTime.now();

	@Column
	private String referencedBy;
	@Column
	private int tokenNo;
	@Column
	private String doctorNotes;
	@Column
	private String result;
	@Column
	private String consultationfee;
	@Column
	private String paid;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "paymentid", referencedColumnName = "paymentid")
	private Payments payments;

	@Transient
	private String appointmentpaymentid;

	public Payments getPayments() {
		return payments;
	}

	public void setPayments(Payments payments) {
		this.payments = payments;
	}

	

	

	

	public String getAppointmentpaymentid() {
		return appointmentpaymentid;
	}

	public void setAppointmentpaymentid(String appointmentpaymentid) {
		this.appointmentpaymentid = appointmentpaymentid;
	}







	@Column(nullable = false)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime CreateTime = ZonedDateTime.now();

	@Column
	private int createdUserid;
	@Column
	private String source;
	@Column
	private String cancelled;
	@Column(nullable = false)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime CancelledTime = ZonedDateTime.now();
	@Column
	private int cancelledUserid;
	@Column
	private String cancelledReason;
	@Column
	private String noshow;
	@Column
	private String arrived;
	@Column
	private String consulted;
	@Column
	private String procedures;
	@Column
	private String reviewsinDays;
	@Column
	private String reviewProcedure;
	@Column
	private String freeFollowup;

	public Long getApptid() {
		return apptid;
	}

	public void setApptid(Long apptid) {
		this.apptid = apptid;
	}

	public ZonedDateTime getApptDate() {
		return apptDate;
	}

	public void setApptDate(ZonedDateTime apptDate) {
		this.apptDate = apptDate;
	}

	public Patients getPatientdetail() {
		return patientdetail;
	}

	public void setPatientdetail(Patients patientdetail) {
		this.patientdetail = patientdetail;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public Clinics getClinic() {
		return clinic;
	}

	public void setClinic(Clinics clinic) {
		this.clinic = clinic;
	}

	public String getSlot() {
		return slot;
	}

	public void setSlot(String slot) {
		this.slot = slot;
	}

	public ZonedDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(ZonedDateTime startTime) {
		this.startTime = startTime;
	}

	public ZonedDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(ZonedDateTime endTime) {
		this.endTime = endTime;
	}

	public String getReferencedBy() {
		return referencedBy;
	}

	public void setReferencedBy(String referencedBy) {
		this.referencedBy = referencedBy;
	}

	public int getTokenNo() {
		return tokenNo;
	}

	public void setTokenNo(int tokenNo) {
		this.tokenNo = tokenNo;
	}

	public String getDoctorNotes() {
		return doctorNotes;
	}

	public void setDoctorNotes(String doctorNotes) {
		this.doctorNotes = doctorNotes;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getConsultationfee() {
		return consultationfee;
	}

	public void setConsultationfee(String consultationfee) {
		this.consultationfee = consultationfee;
	}

	public String getPaid() {
		return paid;
	}

	public void setPaid(String paid) {
		this.paid = paid;
	}

	public ZonedDateTime getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(ZonedDateTime createTime) {
		CreateTime = createTime;
	}

	public int getCreatedUserid() {
		return createdUserid;
	}

	public void setCreatedUserid(int createdUserid) {
		this.createdUserid = createdUserid;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getCancelled() {
		return cancelled;
	}

	public void setCancelled(String cancelled) {
		this.cancelled = cancelled;
	}

	public ZonedDateTime getCancelledTime() {
		return CancelledTime;
	}

	public void setCancelledTime(ZonedDateTime cancelledTime) {
		CancelledTime = cancelledTime;
	}

	public int getCancelledUserid() {
		return cancelledUserid;
	}

	public void setCancelledUserid(int cancelledUserid) {
		this.cancelledUserid = cancelledUserid;
	}

	public String getCancelledReason() {
		return cancelledReason;
	}

	public void setCancelledReason(String cancelledReason) {
		this.cancelledReason = cancelledReason;
	}

	public String getNoshow() {
		return noshow;
	}

	public void setNoshow(String noshow) {
		this.noshow = noshow;
	}

	public String getArrived() {
		return arrived;
	}

	public void setArrived(String arrived) {
		this.arrived = arrived;
	}

	public String getConsulted() {
		return consulted;
	}

	public void setConsulted(String consulted) {
		this.consulted = consulted;
	}

	public String getProcedures() {
		return procedures;
	}

	public void setProcedures(String procedures) {
		this.procedures = procedures;
	}

	public String getReviewsinDays() {
		return reviewsinDays;
	}

	public void setReviewsinDays(String reviewsinDays) {
		this.reviewsinDays = reviewsinDays;
	}

	public String getReviewProcedure() {
		return reviewProcedure;
	}

	public void setReviewProcedure(String reviewProcedure) {
		this.reviewProcedure = reviewProcedure;
	}

	public String getFreeFollowup() {
		return freeFollowup;
	}

	public void setFreeFollowup(String freeFollowup) {
		this.freeFollowup = freeFollowup;
	}

	public Appointments() {
		super();
		// TODO Auto-generated constructor stub
	}

}
