package com.neobone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Specialties {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long specialtyid;
@Column
private String specialtyName;

public Long getSpecialtyid() {
	return specialtyid;
}
public void setSpecialtyid(Long specialtyid) {
	this.specialtyid = specialtyid;
}
public String getSpecialtyName() {
	return specialtyName;
}
public void setSpecialtyName(String specialtyName) {
	this.specialtyName = specialtyName;
}

public Specialties() {
	super();
	// TODO Auto-generated constructor stub
}


}
