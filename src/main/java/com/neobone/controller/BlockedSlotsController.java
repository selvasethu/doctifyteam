package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.BlockedSlots;
import com.neobone.model.Clinics;
import com.neobone.model.Users;
import com.neobone.repository.BlockedSlotsRepo;
import com.neobone.repository.ClinicsRepo;
import com.neobone.repository.UsersRepo;

@Controller
public class BlockedSlotsController {
@Autowired
BlockedSlotsRepo blockedslotsrepo;
@Autowired
UsersRepo usersrepo;
@Autowired
ClinicsRepo clinicsrepo;

@RequestMapping(value="/blockedslot",method=RequestMethod.POST)
@ResponseBody
public BlockedSlots BlockedslotsDtails(BlockedSlots blockedslots) {
	Users getuserid=usersrepo.findById(Long.parseLong(blockedslots.getBlockuserid())).get();
	Clinics getclinicid=clinicsrepo.findById(Long.parseLong(blockedslots.getBlockclinicid())).get();
	blockedslots.setUsers(getuserid);
	blockedslots.setClinic(getclinicid);
	blockedslotsrepo.save(blockedslots);
	return blockedslots;
}


}
