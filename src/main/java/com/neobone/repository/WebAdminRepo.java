package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.WebAdmin;

public interface WebAdminRepo extends JpaRepository<WebAdmin,Long> {

}
