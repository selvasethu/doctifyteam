package com.neobone.model;

import java.time.ZonedDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
public class Payments {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long paymentid;

@Column(nullable = false)
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private ZonedDateTime paymentDate = ZonedDateTime.now();

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="clinicid",referencedColumnName="clinicid")
private Clinics clinics;

@Transient
private String paymentclinicid;



public String getPaymentclinicid() {
	return paymentclinicid;
}

public void setPaymentclinicid(String paymentclinicid) {
	this.paymentclinicid = paymentclinicid;
}



@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="patientsid",referencedColumnName="patientid")
private Patients patients;

@Transient
private String paymentpatientid;



public String getPaymentpatientid() {
	return paymentpatientid;
}

public void setPaymentpatientid(String paymentpatientid) {
	this.paymentpatientid = paymentpatientid;
}



@Column
private double pastDueamount;
@Column
private double totalAmount;
@Column
private double discount;
@Column
private double creditAmount;
@Column 
private double paidAmount;
@Column
private String mode;
@Column
private double cashAmount;
@Column
private double cardAmount;

@Column(nullable = false)
@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
private ZonedDateTime createdTime = ZonedDateTime.now();

@Column
private int crearedUserid;



public ZonedDateTime getPaymentDate() {
	return paymentDate;
}

public void setPaymentDate(ZonedDateTime paymentDate) {
	this.paymentDate = paymentDate;
}



public Long getPaymentid() {
	return paymentid;
}

public void setPaymentid(Long paymentid) {
	this.paymentid = paymentid;
}

public Clinics getClinics() {
	return clinics;
}

public void setClinics(Clinics clinics) {
	this.clinics = clinics;
}

public Patients getPatients() {
	return patients;
}

public void setPatients(Patients patients) {
	this.patients = patients;
}

public double getPastDueamount() {
	return pastDueamount;
}

public void setPastDueamount(double pastDueamount) {
	this.pastDueamount = pastDueamount;
}

public double getTotalAmount() {
	return totalAmount;
}

public void setTotalAmount(double totalAmount) {
	this.totalAmount = totalAmount;
}

public double getDiscount() {
	return discount;
}

public void setDiscount(double discount) {
	this.discount = discount;
}

public double getCreditAmount() {
	return creditAmount;
}

public void setCreditAmount(double creditAmount) {
	this.creditAmount = creditAmount;
}

public double getPaidAmount() {
	return paidAmount;
}

public void setPaidAmount(double paidAmount) {
	this.paidAmount = paidAmount;
}

public String getMode() {
	return mode;
}

public void setMode(String mode) {
	this.mode = mode;
}

public double getCashAmount() {
	return cashAmount;
}

public void setCashAmount(double cashAmount) {
	this.cashAmount = cashAmount;
}

public double getCardAmount() {
	return cardAmount;
}

public void setCardAmount(double cardAmount) {
	this.cardAmount = cardAmount;
}

public ZonedDateTime getCreatedTime() {
	return createdTime;
}

public void setCreatedTime(ZonedDateTime createdTime) {
	this.createdTime = createdTime;
}

public int getCrearedUserid() {
	return crearedUserid;
}

public void setCrearedUserid(int crearedUserid) {
	this.crearedUserid = crearedUserid;
}

public Payments() {
	super();
	// TODO Auto-generated constructor stub
}


}
