package com.neobone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.neobone.model.Usersotp;

public interface UsersotpRepo extends JpaRepository<Usersotp,Long> {

}
