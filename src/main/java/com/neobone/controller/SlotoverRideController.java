package com.neobone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neobone.model.Clinics;
import com.neobone.model.SlotoverRide;
import com.neobone.model.Users;
import com.neobone.repository.ClinicsRepo;
import com.neobone.repository.SlotoverRideRepo;
import com.neobone.repository.UsersRepo;

@Controller
public class SlotoverRideController {
@Autowired
SlotoverRideRepo slotoverriderepo;

@Autowired
UsersRepo usersrepo;

@Autowired
ClinicsRepo clinicsrepo;

@RequestMapping(value="/slotoverridepage", method=RequestMethod.POST)
public String slotoverrideDetail(){
	return "slotoverride";
	
}

@RequestMapping(value="/slotoverride", method=RequestMethod.POST)
@ResponseBody
public SlotoverRide slotoverrideDetails(SlotoverRide slotoverride) {
	Users getuserid=usersrepo.findById(Long.parseLong(slotoverride.getOverRideUserid())).get();
	 Clinics getclinicid=clinicsrepo.findById(Long.parseLong(slotoverride.getOverRideClinicid())).get();
	 slotoverride.setUsers(getuserid);
	 slotoverride.setClinics(getclinicid);
	slotoverriderepo.save(slotoverride);
	return slotoverride;
}
}
