package com.neobone.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class ServiceFees {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long serviceFeesid;

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="clinicid", referencedColumnName="clinicid")
private Clinics clinics;

@ManyToOne(cascade=CascadeType.PERSIST)
@JoinColumn(name="serviceid", referencedColumnName="serviceid")
private Services services;

@Column
private int serviceFees;

public Long getServiceFeesid() {
	return serviceFeesid;
}

public void setServiceFeesid(Long serviceFeesid) {
	this.serviceFeesid = serviceFeesid;
}

public Clinics getClinics() {
	return clinics;
}

public void setClinics(Clinics clinics) {
	this.clinics = clinics;
}

public Services getServices() {
	return services;
}

public void setServices(Services services) {
	this.services = services;
}

public int getServiceFees() {
	return serviceFees;
}

public void setServiceFees(int serviceFees) {
	this.serviceFees = serviceFees;
}

public ServiceFees() {
	super();
	// TODO Auto-generated constructor stub
}



}
